﻿using System;
using System.Diagnostics;

namespace HttpResponseTester {
    class Program {
        static void Main(string[] args) {
            var sw = new Stopwatch();
            new MultiThreadWebRequest().RunTest();
      //      new MyHttpClient().RunTest();
            Console.WriteLine("------------------- warm up done -------------");

            sw.Restart();
            new MultiThreadWebRequest().RunTest();
            sw.Stop();
            Console.WriteLine("Total Completion Time=" + sw.ElapsedMilliseconds + " milliseconds\n");

            //sw.Restart();
            //new MyHttpClient().RunTest();
            //Console.WriteLine("Done in " + sw.ElapsedMilliseconds + " milliseconds\n");

            Console.WriteLine("Program ends");

            Console.ReadLine();
        }
    }
}
