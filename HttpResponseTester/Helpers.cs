﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
// Copyright(c) 2015 Lars Klingsten All Rights Reserved

namespace HttpResponseTester {
    public static class MyExtensions {

        public static string Left(this string str, int stringLength) {
            if (str == null || stringLength <= 0) { return ""; }
            if (stringLength > str.Length) { stringLength = str.Length; }
            return str.Substring(0, stringLength);
        }


        public static void ToConsoleWithTime(this string str) {
            Console.WriteLine(string.Format("{0} {1}", System.DateTime.Now.ToString("hh:mm:ss.fff"), str));
        }

        public static void ToDebugWithTime(this string str) {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} {1}", System.DateTime.Now.ToString("hh:mm:ss.fff"), str));
        }

        public static string RemoveDigits(this string str) {
            return new string(str.Where(char.IsLetter).ToArray()).ToUpper();
        }
        public static string RemoveNonDigits(this string str) {
            return new string(str.Where(char.IsDigit).ToArray()).ToUpper();
        }
    }

    public static class Snippet {
        public static string ReadFile(string path) {
            StreamReader sr = new StreamReader(path);
            System.Text.StringBuilder lines = new System.Text.StringBuilder();
            while (sr.Peek() >= 0) {
                string line = sr.ReadLine();
                lines.Append(line + "\r\n");
            }
            sr.Close();
            return lines.ToString();
        }
    }


    public class MultiThreadHelper {
        private int _threadCount;
        private Object myLock = new Object();

        public void SetThreadCount(int count) {
            _threadCount = count;
        }

        public void ThreadDecreaseCount() {
            lock (myLock) {
                _threadCount--;
            }
        }

        public void AwaitCompletion() {
            while (_threadCount != 0) {
                Thread.Sleep(1);
                // Console.Write(".");
            }
        //    Console.WriteLine(string.Empty);
        }
    }
}
