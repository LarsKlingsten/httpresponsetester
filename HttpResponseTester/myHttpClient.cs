﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace HttpResponseTester {
    class MyHttpClient {

        private HttpClient httpClient = new HttpClient();
        public Dictionary<string, int> _resultDictionary;
        MultiThreadHelper myMultiThreadHelper = new MultiThreadHelper();

        public void RunTest() {
            MyTestClass myTestClass = new MyTestClass().LoadConfig();
            var sw = new Stopwatch();
            decimal totalsMilliSec = 0;
            foreach (TestService ts in myTestClass.Services) {
                if (ts.Enabled) {
                    _resultDictionary = new Dictionary<string, int>();
              
                    sw.Restart();
                    callWebClient(ts);
                    sw.Stop();
                    decimal millisec = decimal.Divide(sw.ElapsedTicks * 1000, Stopwatch.Frequency);
                    totalsMilliSec += millisec;
                    string.Format("Completed in {0:0.00} ms  (or {1} requests per second)", millisec, (int)(ts.TotalRequests * 1000 / millisec)).ToConsoleWithTime();
                    WriteResultDictionary(ts);
                }
            }
            string.Format("MultiThread: Total Commucation Time={0:0.00} ms", totalsMilliSec).ToConsoleWithTime();
        }



        public void callWebClient(TestService ts) {

            myMultiThreadHelper.SetThreadCount(ts.TotalRequests);
            for (int i = 0; i < ts.TotalRequests; i++) {
                CallWeb(ts); ;
            }
            myMultiThreadHelper.AwaitCompletion();
        }

        private async void CallWeb(TestService ts) {
            Task<string> msg = null;
            HttpResponseMessage response = await httpClient.GetAsync(ts.Url);
            if (response.IsSuccessStatusCode) {
                msg = response.Content.ReadAsStringAsync();
            }
            addResultToDictionary(string.Format("status={0} msg={1}", response.StatusCode.ToString(), msg.Result.Left(20)));
            myMultiThreadHelper.ThreadDecreaseCount();
        }

        private Object myLock = new Object();
        public void addResultToDictionary(string httpCode) {
            int exitingResponse = -1;
            lock (myLock) {
                _resultDictionary.TryGetValue(httpCode, out exitingResponse);
                if (exitingResponse == -1) {

                    _resultDictionary.Add(httpCode, 1);
                } else {
                    _resultDictionary[httpCode] = ++exitingResponse;
                }
            }
        }

        private void WriteResultDictionary(TestService testService) {
            foreach (var result in _resultDictionary) {
                string.Format("count={0}  {1} ", result.Value, result.Key).ToConsoleWithTime();
            }
        }
    }
}
