﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;
// Copyright(c) 2015 Lars Klingsten All Rights Reserved

namespace HttpResponseTester {

    class MultiThreadWebRequest {

        private Dictionary<string, int> _resultDictionary;
        MultiThreadHelper myMultiThreadHelper = new MultiThreadHelper();

        public void RunTest() {
            MyTestClass myTestClass = new MyTestClass().LoadConfig();
            var sw = new Stopwatch();
            decimal totalsMilliSec = 0;
            foreach (TestService ts in myTestClass.Services) {
                if (ts.Enabled) {
                    _resultDictionary = new Dictionary<string, int>();

                    string.Format("Thread: req={0} #{1} ({2})", ts.Url, ts.TotalRequests, ts.ServiceName).ToConsoleWithTime();
                    sw.Restart();
                    CallServicesViaThreadPool(ts);
                    sw.Stop();

                    decimal millisec = decimal.Divide(sw.ElapsedTicks * 1000, Stopwatch.Frequency);
                    totalsMilliSec += millisec;
                    string.Format("Completed in {0:0.00} ms  (or {1} requests per second)", millisec,   (int)(ts.TotalRequests * 1000 / millisec)).ToConsoleWithTime();
                   // WriteResultDictionary(ts);
                }
            }
            string.Format("MultiThread: Total Commucation Time={0:0.00} ms", totalsMilliSec).ToConsoleWithTime();
        }

        public void CallServicesViaThreadPool(TestService ts) {
            int MAXTHREADS = 128;

            if (ts.TotalRequests == 1) { // Do not use threads as is not necessary, and is slower for a single request
                GetWebResponse(ts);
            }
            else { 
                myMultiThreadHelper.SetThreadCount(ts.TotalRequests);
                ServicePointManager.DefaultConnectionLimit = MAXTHREADS;
                ThreadPool.SetMaxThreads(MAXTHREADS / 2, MAXTHREADS);
                for (int i = 0; i < ts.TotalRequests; i++) {
                    Task.Run(() => GetWebResponse(ts));
                }
                myMultiThreadHelper.AwaitCompletion();
            }
        }

        public void GetWebResponse(TestService testService) {
            string result;
            WebRequest webRequest = WebRequest.Create(testService.Url);
            webRequest.Proxy = null;
            HttpWebRequest httpWebRequest = (HttpWebRequest)webRequest;
            NetworkCredential myNetworkCredential = new NetworkCredential(testService.ApiKey, "");
            CredentialCache myCredentialCache = new CredentialCache();
            myCredentialCache.Add(testService.Url, "Basic", myNetworkCredential);
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Credentials = myCredentialCache;

            try {
                WebResponse myWebResponse = webRequest.GetResponse();
                HttpWebResponse httpWebResponse = (HttpWebResponse)myWebResponse;
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8);
                string htmlBody = streamReader.ReadToEnd();
                responseStream.Close();
                myWebResponse.Close();
                result = string.Format("response={0} len={1} msg={2}", httpWebResponse.StatusCode.ToString(),htmlBody.Length, htmlBody.Left(40));
            } catch (WebException e) {
                result = e.Message.ToString().Left(60);
            }
            myMultiThreadHelper.ThreadDecreaseCount();
            addResultToDictionary(result);
        }
 
        private static readonly object myLock = new object();
        public void addResultToDictionary(string httpCode) {
            int exitingResponse = -1;
            lock (myLock) {
                _resultDictionary.TryGetValue(httpCode, out exitingResponse);
                if (exitingResponse == -1) {  // existing key/value pair not found. Create a new key/value pair
                    _resultDictionary.Add(httpCode, 1);
                } else {
                    _resultDictionary[httpCode] = ++exitingResponse;
                }
            }
        }

        private void WriteResultDictionary(TestService testService) {
            int count = 0;
            foreach (var result in _resultDictionary) {
                string.Format("count={0}  {1} ", result.Value, result.Key).ToConsoleWithTime();
                count += result.Value;
            }
            Console.WriteLine();
        }
    }
}